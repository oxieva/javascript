import '../assets/css/style.css';

const app = document.getElementById('app');
app.innerHTML = '<h1>JavaScript Basics</h1> <h2>Welcome to the jungle</h2>';


/* //Function declarations and expressions

// hoisting
console.log(makeCar);
console.log(makeCarExpression);
console.log(makeCarArrow);
console.log(makeCarArrowShorthand);

// 1: Function Declaration
function makeCar() {
  console.log('Making car...');
}
console.log(makeCar);

makeCar();

// 2: Function Expression (anonymous or named)
// Funció anònima
const makeCarExpression = function () {
  console.log('Hio, makeCarExpression');
};

makeCarExpression();

console.log(makeCarExpression.name);

const makeCarExpressio = function myFunction () {};

console.log(makeCarExpressio.name);

// 3: Arrow Function
// Des de ES2015
const makeCarArrow = () => {
  console.log('Making car inside Arrow...');
};

makeCarArrow();

const makeCarArrowShorthand = () => console.log('Short');

makeCarArrowShorthand(); */

// Function parameters and defaults
/* // name = parameter Quan declarem la funció
function makeCar(name = 'Subaru' ) {
  // name = name || 'Subaru';

  // if (!name) {
  //   name = 'Subaru';
  // }
  console.log(`Making car: ${name.toUpperCase()}`);
}

// strings = arguments Quan utilitzem la funció
makeCar('Porsche');
makeCar('Ferrari');
makeCar(); */


// Rest Parameters and arguments
// Els arguments formen com un array, però no és un array!!! 
function makeCarPrice() {
  console.log(arguments, Array.isArray(arguments)); //per això dona false És un objecte
  console.log(arguments[1]);
  // Convertim els arguments en un array
  console.log(Array.from(arguments));
  // Printem tots els items del array
  Array.from(arguments).forEach(value=> console.log(value));

  //Fem la suma dels items
  const total = Array.from(arguments).reduce((prev, next) => {
    return prev + next;
  });

  console.log(`Total: ${total}Euros`);
}

makeCarPrice(11, 44, 55, 99, 66, 22);

//Des de ES2015 es fa així
function makeCarPriceNumber(numberOne, ...params) {
  console.log(numberOne, params); // És un array
  console.log(Array.isArray(params)); // True
  const total = params.reduce((prev, next) => prev + next);
  console.log(total);
}

function makeCarPriceRest(...params) {
  console.log(Array.isArray(params));
  const total = params.reduce((prev, next) => prev + next);
  console.log(`Total: ${total}Euros`);
}

makeCarPriceNumber(99, 88, 77, 11, 44);
makeCarPriceRest(99, 88, 77, 11, 44);
